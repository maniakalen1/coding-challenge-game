<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public $questions = [];

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        $questionsJson = $this->json->decodeFile(static::QUESTIONS_PATH);
        foreach ($questionsJson as &$questionData) {
            $this->questions[$questionData->id] = new Question($questionData);

        }
    }

    /**
     * Facilitates question fetch by id
     *
     * @param int $id
     * @return Question
     * @throws \Exception
     */
    public function getQuestionWithId(int $id): Question
    {
        if (!isset($this->questions[$id])) {
            throw new \Exception("Question not found");
        }
        return $this->questions[$id];
    }

    /** Retreives the required count of question ids in random order */
    public function getRandomQuestions(int $count = 5): array
    {
        $questions = array_keys($this->questions);
        shuffle($questions);
        $chunked = array_chunk($questions, $count, true);
        return $chunked[0];
    }

    /**
     * Checks the answer for the question and returns score. 0 points if answer is wrong.
     *
     * @param int $id
     * @param string $answer
     * @return int
     *
     * @throws \Exception
     */
    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->getQuestionWithId($id);
        if ($answer === $question->getCorrectAnswer()) {
            return $question->getPoints();
        }
        return 0;
    }
}