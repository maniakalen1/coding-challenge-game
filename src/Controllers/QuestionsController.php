<?php


namespace Ucc\Controllers;


use Ucc\Http\JsonResponseTrait;
use Ucc\Models\Question;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    /**
     * We use this glue in our implode/explode functions to control the questions flow
     */
    const QUESTIONS_ORDER_GLUE = '|';

    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('points', 0);
        Session::set('questionCount', 1);
        $questions = $this->questionService->getRandomQuestions();
        $firstQuestionId = array_shift($questions);
        Session::set('questionsOrder', implode(static::QUESTIONS_ORDER_GLUE, $questions));
        try {
            $question = $this->questionService->getQuestionWithId($firstQuestionId);
        } catch (\Exception $e) {
            return $this->json('Failed to find question',500);
        }
        return $this->json(['question' => $question->jsonSerialize()], 201);
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            return $this->json('You must first begin a game', 400);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        try {
            if (($points = $this->questionService->getPointsForAnswer($id, $answer)) !== 0) {
                $pointsInSession = (int)Session::get('points');
                $pointsInSession += $points;
                Session::set('points', (string)$pointsInSession);
            }
        } catch (\Exception $ex) {
            return $this->json(['message' => 'An error occurred'], 500);
        }
        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = (int)Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }
        /** Incrementing the question's sequence */
        $count = (int)Session::get('questionCount');
        $count++;
        Session::set('questionCount', $count);
        /** End incrementation */

        /** Fetch next question and prepare final message */
        $message = 'Thank you for your answer. Next question is: ';
        $questionsOrder = Session::get('questionsOrder');
        $questionsOrder = explode(static::QUESTIONS_ORDER_GLUE, $questionsOrder);
        $nextQuestionId = array_shift($questionsOrder);
        Session::set('questionsOrder', implode(static::QUESTIONS_ORDER_GLUE, $questionsOrder));
        try {
            $question = $this->questionService->getQuestionWithId($nextQuestionId);
        } catch (\Exception $ex) {
            return $this->json(['message' => 'An error occurred'], 500);
        }
        return $this->json(['message' => $message, 'question' => $question]);
    }
}